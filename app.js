const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");
const misRutas = require("./router/index.js");
const path = require("path");


const app = express();
app.set('view engine', "ejs")
app.use(express.static(__dirname + '/public'));
app.engine('html',require('ejs').renderFile);

app.use(bodyparser.urlencoded({extended:true}));
app.use(misRutas);

app.use((req,res,next)=>{
res.status(404).sendFile(__dirname+ '/views/error.html');
})

// escuchar el servidor por el puerto 501
const puerto = 501;
app.listen(puerto,()=>{

console.log("Iniciado puerto 501");

});